package com.se.job.service;

import com.se.job.pojo.Equipment;
import com.se.job.pojo.Workshop;
import com.se.job.utils.ResultData;

import java.util.List;

/**
 *
 *
 */
public interface WorkshopService {

    List<Workshop> listWorkshops();

    List<Workshop> getWorkshops(String keyword);

    ResultData addWorkshop(Workshop workshop);

    ResultData delWorkshop(Integer id);

    List<Equipment> listEquipmentsByWid(Integer id);

    Workshop getWorkshopById(Integer id);

    ResultData updWorkshop(Workshop workshop);

    List<Workshop> listFreeWorkshops();
}
