package com.se.job.pojo;

/**
 *
 *
 */
public class Workshop {

    private Integer id;
    private String wname;
    private Integer eid;
    private String ename;

    public Workshop() {
    }

    public Workshop(Integer id, String wname, Integer eid) {
        this.id = id;
        this.wname = wname;
        this.eid = eid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWname() {
        return wname;
    }

    public void setWname(String wname) {
        this.wname = wname;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    @Override
    public String toString() {
        return "Workshop{" +
                "id=" + id +
                ", wname='" + wname + '\'' +
                ", eid=" + eid +
                ", ename='" + ename + '\'' +
                '}';
    }
}
