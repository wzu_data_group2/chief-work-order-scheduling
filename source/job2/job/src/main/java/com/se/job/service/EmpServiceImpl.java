package com.se.job.service;

import com.se.job.mapper.EmpMapper;
import com.se.job.pojo.Emp;
import com.se.job.utils.ResultData;
import com.se.job.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 *
 */
@Service
public class EmpServiceImpl implements EmpService {

    @Autowired
    private EmpMapper empMapper;

    @Override
    public ResultData register(Emp emp) {

        Emp user = empMapper.getEmpByName(emp.getName());
        if(user != null){
            return new ResultData(false,"用户名已存在");
        }
        if(emp.getPwd().isEmpty()){
            return new ResultData(false,"密码不能为空");
        }
        String salt = ShiroUtils.getSalt(8);
        emp.setSalt(salt);
        emp.setPwd(ShiroUtils.encry(emp.getPwd(), salt));
        empMapper.addEmp(emp);
        return new ResultData(true);
    }

    @Override
    public Emp login(String name) {
        return empMapper.getEmpByName(name);
    }

    @Override
    public List<Emp> listEmps() {
        return empMapper.getEmps();
    }
}
