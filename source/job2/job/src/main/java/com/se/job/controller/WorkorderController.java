package com.se.job.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.se.job.pojo.Equipment;
import com.se.job.pojo.Workorder;
import com.se.job.pojo.Workshop;
import com.se.job.service.EquipmentService;
import com.se.job.service.WorkorderService;
import com.se.job.service.WorkshopService;
import com.se.job.utils.DataUtils;
import com.se.job.utils.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 */
@Controller
public class WorkorderController {

    @Autowired
    private WorkorderService workorderService;

    @Autowired
    private EquipmentService equipmentService;

    @Autowired
    private WorkshopService workshopService;

    //    前往工单页面
    @RequestMapping("/goWorkorder")
    public String goWorkorder(String keyword, Integer pageNum, Integer pageSize,Model model){

        List<Workshop> workshops = workshopService.listFreeWorkshops();

        if(pageNum == null){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 10;
        }
        PageHelper.startPage(pageNum, pageSize);
        List<Workorder> workorders = workorderService.getWorkorders(keyword);
        PageInfo<Workorder> pageInfo = new PageInfo<>(workorders);

        if(pageNum == 1){
            pageInfo.setPrePage(1);
        }else if(pageInfo.getNextPage() == 0){
            pageInfo.setNextPage(pageInfo.getPrePage() + 1);
        }

        model.addAttribute("workorders", workorders);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("workshops",workshops);

        return "workorder";
    }

    @RequestMapping("/addWorkorder")
    public String addWorkorder(Workorder workorder,Model model){
        ResultData res = workorderService.addWorkorder(workorder);
        if(!res.getStatus()){
            model.addAttribute("message", res.getMessage());
        }
        return "redirect:/goWorkorder";
    }

    @RequestMapping("/confirm")
    public String confirm(Integer id,Integer eid, Integer finish, Model model){
        ResultData res = workorderService.confirm(id, finish);
        equipmentService.updEquipmentState(eid, 0);
        if(!res.getStatus()){
            model.addAttribute("message", res.getMessage());
        }
        return "redirect:/goWorkorder";
    }

    @RequestMapping("/delWorkorder/{id}")
    public String delWorkorder(@PathVariable("id") Integer id,Model model){
        ResultData res = workorderService.delWorkorder(id);
        if(!res.getStatus()){
            model.addAttribute("message", res.getMessage());
        }
        return "redirect:/goWorkorder";
    }

    @RequestMapping("/free")
    @ResponseBody
    public Map<Object,Object> free(Integer wid){
        List<Equipment> equipments = equipmentService.listFreeEquipments(wid);
        HashMap<Object, Object> map = new HashMap<>();
        map.put("equipments", equipments);
        return map;
    }

    @RequestMapping("/send")
    public String send(Integer id,Integer wid,Integer eid,Model model){
        ResultData res = workorderService.send(id,wid,eid);
        ResultData res2 = equipmentService.updEquipmentState(eid, 1);
        if(!res.getStatus()){
            model.addAttribute("message", res.getMessage());
        }
        return "redirect:/goWorkorder";
    }

    @RequestMapping("/export")
    @ResponseBody
    public void export(String keyword, HttpServletResponse response){
        List<Workorder> workorders = workorderService.getWorkorders(keyword);
        DataUtils.exportWorkorder(response, workorders);
    }

}
