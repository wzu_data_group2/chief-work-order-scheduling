package com.se.job.service;

import com.se.job.pojo.Workorder;
import com.se.job.utils.ResultData;

import java.util.List;

/**
 *
 *
 */
public interface WorkorderService {
    List<Workorder> getWorkorders(String keyword);

    ResultData addWorkorder(Workorder workorder);

    ResultData confirm(Integer id,Integer finish);

    ResultData delWorkorder(Integer id);

    ResultData send(Integer id,Integer wid,Integer eid);

    void sendTimer();
}
