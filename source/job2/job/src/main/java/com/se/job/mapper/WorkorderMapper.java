package com.se.job.mapper;

import com.se.job.pojo.Workorder;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 *
 */
@Repository
public interface WorkorderMapper {
    List<Workorder> getWorkordersByName(String keyword);

    int addWorkorder(Workorder workorder);

    int confirm(@Param("id") Integer id,@Param("finish") Integer finish);

    int delWorkorder(Integer id);

    int send(@Param("id") Integer id,@Param("wid") Integer wid,@Param("eid") Integer eid);

    List<Integer> getUnSentWorkordersId();
}
