package com.se.job.utils;

import com.se.job.pojo.Workorder;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 *
 *
 */
public class DataUtils {
    public static void exportWorkorder(HttpServletResponse response,List<Workorder> list){

        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet();
        HSSFRow row;

        //表头
        row = sheet.createRow(0);
        row.createCell(0).setCellValue("工单号");
        row.createCell(1).setCellValue("项目名");
        row.createCell(2).setCellValue("加工物品名称");
        row.createCell(3).setCellValue("生产数量");
        row.createCell(4).setCellValue("设备名");
        row.createCell(5).setCellValue("车间名");
        row.createCell(6).setCellValue("是否派单");
        row.createCell(7).setCellValue("是否完成");

        //数据
        for (int i = 0; i < list.size(); i++) {
            row = sheet.createRow(i+1);
            Workorder workorder = list.get(i);
            row.createCell(0).setCellValue(workorder.getId());
            row.createCell(1).setCellValue(workorder.getName());
            row.createCell(2).setCellValue(workorder.getItem());
            row.createCell(3).setCellValue(workorder.getNum());
            row.createCell(4).setCellValue(workorder.getEname());
            row.createCell(5).setCellValue(workorder.getWname());
            row.createCell(6).setCellValue(workorder.getIsSend()==1?"是":"否");
            row.createCell(7).setCellValue(workorder.getIsFinish()==1?"是":"否");
        }

        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        try {
            OutputStream os = response.getOutputStream();
            response.setHeader("Content-disposition","attachment;filename=workorder.xls");//默认Excel名称
            wb.write(os);
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
