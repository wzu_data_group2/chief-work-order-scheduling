package com.se.job.pojo;

/**
 *
 *
 */
public class Workorder {

    private Integer id;
    private String name;
    private String item;
    private Integer num;
    private Integer eid;
    private String ename;
    private Integer wid;
    private String wname;
    private Integer isSend;
    private Integer isFinish;

    public Workorder() {
    }

    public Workorder(String name, String item, Integer num) {
        this.name = name;
        this.item = item;
        this.num = num;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public Integer getWid() {
        return wid;
    }

    public void setWid(Integer wid) {
        this.wid = wid;
    }

    public Integer getIsSend() {
        return isSend;
    }

    public void setIsSend(Integer isSend) {
        this.isSend = isSend;
    }

    public Integer getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(Integer isFinish) {
        this.isFinish = isFinish;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getWname() {
        return wname;
    }

    public void setWname(String wname) {
        this.wname = wname;
    }

    @Override
    public String toString() {
        return "Workorder{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", item='" + item + '\'' +
                ", num=" + num +
                ", eid=" + eid +
                ", ename='" + ename + '\'' +
                ", wid=" + wid +
                ", wname='" + wname + '\'' +
                ", isSend=" + isSend +
                ", isFinish=" + isFinish +
                '}';
    }
}
