package com.se.job.mapper;

import com.se.job.pojo.Equipment;
import com.se.job.pojo.Workshop;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 *
 */
@Repository
public interface WorkshopMapper {
    List<Workshop> getAllWorkshops();

    List<Workshop> getWorkshopsByWname(String keyword);

    int addWorkshop(Workshop workshop);

    int delWorkshop(Integer id);

    List<Equipment> getWorkshopEquipmentsById(Integer id);

    Workshop getWorkshopById(Integer id);

    int updWorkshop(Workshop workshop);

    List<Workshop> getFreeWorkshops();

}
