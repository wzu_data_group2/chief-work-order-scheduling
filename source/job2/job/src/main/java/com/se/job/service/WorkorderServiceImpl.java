package com.se.job.service;

import com.se.job.mapper.EquipmentMapper;
import com.se.job.mapper.WorkorderMapper;
import com.se.job.pojo.Equipment;
import com.se.job.pojo.Workorder;
import com.se.job.utils.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 *
 */
@Service
public class WorkorderServiceImpl implements WorkorderService {

    @Autowired
    private WorkorderMapper workorderMapper;

    @Autowired
    private EquipmentMapper equipmentMapper;

    @Override
    public List<Workorder> getWorkorders(String keyword) {
        return workorderMapper.getWorkordersByName(keyword);
    }

    @Override
    public ResultData addWorkorder(Workorder workorder) {
        if(workorderMapper.addWorkorder(workorder) != 0){
            return new ResultData(true);
        }
        return new ResultData(false,"增加失败");
    }

    @Override
    public ResultData confirm(Integer id,Integer finish) {
        if(workorderMapper.confirm(id,finish) != 0){
            return new ResultData(true);
        }
        return new ResultData(false,"确认失败");
    }

    @Override
    public ResultData delWorkorder(Integer id) {
        if(workorderMapper.delWorkorder(id) != 0){
            return new ResultData(true);
        }
        return new ResultData(false,"删除失败");
    }

    @Override
    public ResultData send(Integer id, Integer wid, Integer eid) {
        if(workorderMapper.send(id, wid, eid) != 0){
            return new ResultData(true);
        }
        return new ResultData(false,"派单失败");
    }

    @Override
    @Async
    @Scheduled(cron = "0 0 0/1 * * ?")
    public void sendTimer(){
        List<Integer> ids = workorderMapper.getUnSentWorkordersId();
        List<Equipment> equipments = equipmentMapper.getFreeEquipments();
        for (int i = 0; i < ids.size(); i++) {
            Integer id = ids.get(i);
            Equipment equipment = equipments.get(i);
            System.out.println(ids);
            System.out.println(equipment);
            send(id, equipment.getWid(),equipment.getId());
            equipmentMapper.updEquipmentState(equipment.getId(), 1);
        }
    }
}
