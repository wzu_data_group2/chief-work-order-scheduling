package com.se.job.service;

import com.se.job.pojo.Equipment;
import com.se.job.utils.ResultData;

import java.util.List;

/**
 *
 *
 */
public interface EquipmentService {
    ResultData addEquipment(Equipment equipment);

    ResultData delEquipment(Integer id);

    ResultData updEquipment(Equipment equipment);

    List<Equipment> getEquipments(String keyword);

    Equipment getEquipmentById(Integer id);

    List<Equipment> listFreeEquipments(Integer wid);

    ResultData updEquipmentState(Integer id,Integer state);
}
