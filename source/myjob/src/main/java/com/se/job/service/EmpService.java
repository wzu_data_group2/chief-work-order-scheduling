package com.se.job.service;

import com.se.job.pojo.Emp;
import com.se.job.utils.ResultData;

import java.util.List;

/**
 *
 *
 */
public interface EmpService {
    ResultData register(Emp emp);

    Emp login(String name);

    List<Emp> listEmps();
}
