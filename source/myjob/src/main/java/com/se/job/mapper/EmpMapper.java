package com.se.job.mapper;

import com.se.job.pojo.Emp;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 *
 */
@Repository
public interface EmpMapper {
    void addEmp(Emp emp);

    Emp getEmpByName(String name);

    List<Emp> getEmps();
}
