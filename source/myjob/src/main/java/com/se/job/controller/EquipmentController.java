package com.se.job.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.se.job.pojo.Emp;
import com.se.job.pojo.Equipment;
import com.se.job.pojo.Workshop;
import com.se.job.service.EmpService;
import com.se.job.service.EquipmentService;
import com.se.job.service.WorkshopService;
import com.se.job.utils.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 */
@Controller
public class EquipmentController {

    @Autowired
    private EquipmentService equipmentService;

    @Autowired
    private WorkshopService workshopService;

    @Autowired
    private EmpService empService;

    //    前往设备页面
    @RequestMapping("/goEquipment")
    public String goEquipment(String keyword, Integer pageNum, Integer pageSize, Model model){
        if(pageNum == null){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 10;
        }
        PageHelper.startPage(pageNum, pageSize);
        List<Equipment> equipments = equipmentService.getEquipments(keyword);
        PageInfo<Equipment> pageInfo = new PageInfo<>(equipments);

        if(pageNum == 1){
            pageInfo.setPrePage(1);
        }else if(pageInfo.getNextPage() == 0){
            pageInfo.setNextPage(pageInfo.getPrePage() + 1);
        }

        List<Workshop> workshops = workshopService.listWorkshops();
        List<Emp> emps = empService.listEmps();

        model.addAttribute("emps",emps);
        model.addAttribute("workshops", workshops);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("pageInfo", pageInfo);
        model.addAttribute("equipments", equipments);

        return "equipment";
    }

    @RequestMapping("/addEquipment")
    public String addEquipment(Equipment equipment,Model model){
        ResultData res = equipmentService.addEquipment(equipment);
        if(!res.getStatus()){
            model.addAttribute("message",res.getMessage());
        }
        return "forward:/goEquipment";
    }

    @RequestMapping("/delEquipment/{id}")
    public String delEquipment(@PathVariable("id") Integer id,Model model){
        ResultData res = equipmentService.delEquipment(id);
        if(!res.getStatus()){
            model.addAttribute("message",res.getMessage());
        }
        return "redirect:/goEquipment";
    }

    @RequestMapping("/updEquipment/{id}")
    @ResponseBody
    public Map<Object, Object> updEquipment(@PathVariable("id") Integer id){
        Equipment equipment = equipmentService.getEquipmentById(id);
        HashMap<Object, Object> map = new HashMap<>();
        map.put("equipment", equipment);
        return map;
    }

    @RequestMapping("/updEquipment")
    public String updEquipment(Equipment equipment, Model model){
        ResultData res = equipmentService.updEquipment(equipment);
        if(!res.getStatus()){
            model.addAttribute("message",res.getMessage());
        }
        return "forward:/goEquipment";
    }
}
