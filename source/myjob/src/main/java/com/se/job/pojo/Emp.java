package com.se.job.pojo;

/**
 *
 *
 */
public class Emp {
    private Integer id;
    private String name;
    private String pwd;
    private String salt;
    private Integer role;
    private Integer wid;

    public Emp() {
    }

    public Emp(Integer id, String name, String pwd, String salt, Integer role, Integer wid) {
        this.id = id;
        this.name = name;
        this.pwd = pwd;
        this.salt = salt;
        this.role = role;
        this.wid = wid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Integer getWid() {
        return wid;
    }

    public void setWid(Integer wid) {
        this.wid = wid;
    }
}
