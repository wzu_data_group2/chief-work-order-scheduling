package com.se.job.service;

import com.se.job.mapper.WorkshopMapper;
import com.se.job.pojo.Equipment;
import com.se.job.pojo.Workshop;
import com.se.job.utils.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 *
 */
@Service
public class WorkshopServiceImpl implements WorkshopService {

    @Autowired
    private WorkshopMapper workshopMapper;

    @Override
    public List<Workshop> listWorkshops() {
        return workshopMapper.getAllWorkshops();
    }

    @Override
    public List<Workshop> getWorkshops(String keyword) {
        return workshopMapper.getWorkshopsByWname(keyword);
    }

    @Override
    public ResultData addWorkshop(Workshop workshop) {
        if(workshopMapper.addWorkshop(workshop) != 0){
            return new ResultData(true);
        }
        return new ResultData(false,"增加失败");
    }

    @Override
    public ResultData delWorkshop(Integer id) {
        if(workshopMapper.delWorkshop(id) != 0){
            return new ResultData(true);
        }

        return new ResultData(false,"增加失败");
    }

    @Override
    public List<Equipment> listEquipmentsByWid(Integer id) {
        return workshopMapper.getWorkshopEquipmentsById(id);
    }

    @Override
    public Workshop getWorkshopById(Integer id) {
        return workshopMapper.getWorkshopById(id);
    }

    @Override
    public ResultData updWorkshop(Workshop workshop) {
        if(workshopMapper.updWorkshop(workshop) != 0){
            return new ResultData(true);
        }
        return new ResultData(false,"修改失败");
    }

    @Override
    public List<Workshop> listFreeWorkshops() {
        return workshopMapper.getFreeWorkshops();
    }
}
