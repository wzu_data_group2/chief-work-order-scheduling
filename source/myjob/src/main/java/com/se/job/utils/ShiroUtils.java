package com.se.job.utils;

import org.apache.shiro.crypto.hash.Md5Hash;

import java.util.Random;

public class ShiroUtils {

    /**
     * 生成salt的静态方法
     * @param n
     * @return
     */
    public static String getSalt(int n){
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890!@#$%^&*()"
                .toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            char aChar = chars[new Random().nextInt(chars.length)];
            sb.append(aChar);
        }
        return sb.toString();
    }

    public static String encry(String pwd,String salt){
        return new Md5Hash(pwd,salt,1024).toHex();
    }
}
