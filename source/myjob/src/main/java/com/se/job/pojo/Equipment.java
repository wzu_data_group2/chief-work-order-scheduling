package com.se.job.pojo;

/**
 *
 *
 */
public class Equipment {

    private Integer id;
    private String ename;
    private String name;
    private String wname;
    private Integer state;
    private Integer eid;
    private Integer wid;

    public Equipment() {
    }

    public Equipment(Integer id, String ename, Integer state, Integer eid, Integer wid) {
        this.id = id;
        this.ename = ename;
        this.state = state;
        this.eid = eid;
        this.wid = wid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public Integer getWid() {
        return wid;
    }

    public void setWid(Integer wid) {
        this.wid = wid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWname() {
        return wname;
    }

    public void setWname(String wname) {
        this.wname = wname;
    }

    @Override
    public String toString() {
        return "Equipment{" +
                "id=" + id +
                ", ename='" + ename + '\'' +
                ", name='" + name + '\'' +
                ", wname='" + wname + '\'' +
                ", state=" + state +
                ", eid=" + eid +
                ", wid=" + wid +
                '}';
    }
}
