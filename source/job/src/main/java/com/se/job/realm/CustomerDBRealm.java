package com.se.job.realm;

import com.se.job.pojo.Emp;
import com.se.job.service.EmpService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

public class CustomerDBRealm extends AuthorizingRealm {

    @Autowired
    private EmpService empService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

        System.out.println("授权什么时候调用。。。。。。。。。。。");
        //获取身份信息
        String primaryPrincipal = (String) principals.getPrimaryPrincipal();
        System.out.println("调用授权验证: "+primaryPrincipal);
        Emp emp = empService.login(primaryPrincipal);

        System.out.println(emp);
        //授权角色信息
        if(!StringUtils.isEmpty(emp.getRole().toString())){

            SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();


            simpleAuthorizationInfo.addRole(emp.getRole().toString()); //添加角色信息
            return simpleAuthorizationInfo;
        }
        return null;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {

        System.out.println("================================");
        //根据身份信息//从传过来的token获取到的用户名
        String principal = (String) token.getPrincipal();
        //根据身份信息查询
        Emp emp = empService.login(principal);

        //用户不为空
        if(!ObjectUtils.isEmpty(emp)){
            //返回数据库信息
            SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(
                    emp.getName(),
                    emp.getPwd(),
                    ByteSource.Util.bytes(emp.getSalt()),
                    this.getName());
            return simpleAuthenticationInfo;
        }
        return null;
    }
}
