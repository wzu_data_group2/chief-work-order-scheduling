package com.se.job.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.se.job.pojo.Emp;
import com.se.job.pojo.Equipment;
import com.se.job.pojo.Workshop;
import com.se.job.service.EmpService;
import com.se.job.service.WorkshopService;
import com.se.job.utils.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 */
@Controller
public class WorkshopController {

    @Autowired
    private WorkshopService workshopService;

    @Autowired
    private EmpService empService;

    //    前往车间页面
    @RequestMapping("/goWorkshop")
    public String goWorkshop(String keyword, Integer pageNum,Integer pageSize,Model model){
        if(pageNum == null){
            pageNum = 1;
        }
        if(pageSize == null){
            pageSize = 10;
        }
        PageHelper.startPage(pageNum, pageSize);
        List<Workshop> workshops = workshopService.getWorkshops(keyword);
        PageInfo<Workshop> pageInfo = new PageInfo<>(workshops);

        if(pageNum == 1){
            pageInfo.setPrePage(1);
        }else if(pageInfo.getNextPage() == 0){
            pageInfo.setNextPage(pageInfo.getPrePage() + 1);
        }

        List<Emp> emps = empService.listEmps();

        model.addAttribute("emps",emps);
        model.addAttribute("workshops", workshops);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("pageInfo", pageInfo);

        return "workshop";
    }

    @RequestMapping("/addWorkshop")
    public String addWorkshop(Workshop workshop,Model model){
        ResultData res = workshopService.addWorkshop(workshop);
        if(!res.getStatus()){
            model.addAttribute("message", res.getMessage());
        }
        return "forward:/goWorkshop";
    }

    @RequestMapping("/delWorkshop/{id}")
    public String delWorkshop(@PathVariable("id") Integer id, Model model){
        ResultData res = workshopService.delWorkshop(id);
        if(!res.getStatus()){
            model.addAttribute("message",res.getMessage());
        }
        return "redirect:/goWorkshop";
    }

    @RequestMapping("/updWorkshop/{id}")
    @ResponseBody
    public Map<Object, Object> updWorkshop(@PathVariable("id") Integer id){
        Workshop workshop = workshopService.getWorkshopById(id);
        HashMap<Object, Object> map = new HashMap<>();
        map.put("workshop", workshop);
        return map;
    }

    @RequestMapping("/updWorkshop")
    public String updWorkshop(Workshop workshop,Model model){
        ResultData res = workshopService.updWorkshop(workshop);
        if(!res.getStatus()){
            model.addAttribute("message", res.getMessage());
        }
        return "forward:/goWorkshop";
    }

    @RequestMapping("/detail/{id}")
    @ResponseBody
    public Map<Object,Object> detail(@PathVariable("id") Integer id){
        List<Equipment> details = workshopService.listEquipmentsByWid(id);
        HashMap<Object, Object> map = new HashMap<>();
        map.put("details", details);
        return map;
    }

}
