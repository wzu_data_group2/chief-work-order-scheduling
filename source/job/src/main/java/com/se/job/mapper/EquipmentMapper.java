package com.se.job.mapper;

import com.se.job.pojo.Equipment;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 *
 */
@Repository
public interface EquipmentMapper {
    int addEquipment(Equipment equipment);

    int delEquipment(Integer id);

    int updEquipment(Equipment equipment);

    List<Equipment> getEquipments(String keyword);

    Equipment getEquipmentById(Integer id);

    List<Equipment> getFreeEquipmentsByWid(Integer wid);

    int updEquipmentState(@Param("id") Integer id,@Param("state") Integer state);

    List<Equipment> getFreeEquipments();
}
