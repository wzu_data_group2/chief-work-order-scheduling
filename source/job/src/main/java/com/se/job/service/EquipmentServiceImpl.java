package com.se.job.service;

import com.se.job.mapper.EquipmentMapper;
import com.se.job.pojo.Equipment;
import com.se.job.utils.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 *
 */
@Service
public class EquipmentServiceImpl implements EquipmentService {

    @Autowired
    EquipmentMapper equipmentMapper;

    @Override
    public ResultData addEquipment(Equipment equipment) {
        if(equipmentMapper.addEquipment(equipment) != 0){
            return new ResultData(true);
        }
        return new ResultData(false,"增加失败");
    }

    @Override
    public ResultData delEquipment(Integer id) {
        if(equipmentMapper.delEquipment(id) != 0){
            return new ResultData(true);
        }
        return new ResultData(false,"删除失败");
    }

    @Override
    public ResultData updEquipment(Equipment equipment) {
        if(equipmentMapper.updEquipment(equipment) != 0){
            return new ResultData(true);
        }
        return new ResultData(false,"修改失败");
    }

    @Override
    public List<Equipment> getEquipments(String keyword) {
        return equipmentMapper.getEquipments(keyword);
    }

    @Override
    public Equipment getEquipmentById(Integer id) {
        return equipmentMapper.getEquipmentById(id);
    }

    @Override
    public List<Equipment> listFreeEquipments(Integer wid) {
        return equipmentMapper.getFreeEquipmentsByWid(wid);
    }

    @Override
    public ResultData updEquipmentState(Integer id, Integer state) {
        if(equipmentMapper.updEquipmentState(id,state) != 0){
            return new ResultData(true);
        }
        return new ResultData(false,"更新失败");
    }

}
