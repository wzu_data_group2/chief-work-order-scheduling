# 主任工单排程

#### 功能介绍
1. 分角色登录、注册
1. 设备管理： 增，删，改，查。
2. 车间管理： 增，删，改，查，查看车间设备。
3. 工单管理： 增，删，查，派单，确认完成，导出Excel文件。

#### 第二小组成员

1.  产品经理 应宇鸿
2.  项目经理 周琳琳
3.  后端程序员 宣熠
4.  前端程序员 张依媚
5.  测试人员 赵锦澳



